//
// Created by enetheru on 16/4/20.
//

#include "httpws.h"
#include "httpws_handle_sdfat.h"

//FIXME move to somewhere relevant
#ifdef ARDUINO
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

static int freeMemory(){
    char top;
#ifdef __arm__
    return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
    return &top - __brkval;
#else  // __arm__
    return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__

}
#endif //ARDUINO

namespace httpws{

    HandleSdFat::HandleSdFat( Client *client ){
        this->client = client;

        Serial.println( F( "Setting Up SD Card" ) );
        if( SD.begin( 4, SD_SCK_MHZ( 50 ) ) )return;

        Serial.println( F( "SD CARD: initialization failed" ) );
        if( SD.cardErrorCode() ){
            Serial.print( F("errorCode: ") );
            Serial.println( String( SD.cardErrorCode(), HEX ) );
            Serial.print( F("errorData: "));
            Serial.println( String( SD.cardErrorData(), HEX ) );
            return;
        }
        if( SD.fatType() == 0 ){
            Serial.println( F( "Can't find a valid FAT16/FAT32 partition" ) );
            return;
        }
        if( !SD.vwd()->isOpen() ){
            Serial.println( F( "Can't open root directory" ) );
            return;
        }
    }

    bool HandleSdFat::error(){
        if( SD.cardErrorCode() || !SD.vwd()->isOpen() ) return true;
        return false;
    }

    Handle *HandleSdFat::factory( Client *client ){
        auto *handle = new HandleSdFat(client);
        if( handle->error() ){
            client->httpResponse( 500, F("Unable to Initialise SD Card") );
            delete handle;
            return nullptr;
        }

        switch( client->getMethod() ){
            case Method::GET:
                handle->sdfatAction = &HandleSdFat::httpGET;
                break;
            case Method::PUT:
                handle->sdfatAction = &HandleSdFat::httpPUT;
                break;
            case Method::POST:
                handle->sdfatAction = &HandleSdFat::httpPOST;
                break;
            case Method::DELETE:
                handle->sdfatAction = &HandleSdFat::httpDELETE;
                break;
            default:
                client->httpResponse( 501, F("Unsupported HTTP Method") );
                delete handle;
                return nullptr;
        }
        return handle;
    }

    OpCode HandleSdFat::resume(){
        return (this->*sdfatAction)();
    }

    OpCode HandleSdFat::httpGET(){
/* https://tools.ietf.org/html/rfc7231#section-5
5.  Request Header Fields

   A client sends request header fields to provide more information
   about the request context, make the request conditional based on the
   target resource state, suggest preferred formats for the response,
   supply authentication credentials, or modify the expected request
   processing.  These fields act as request modifiers, similar to the
   parameters on a programming language method invocation.
*/
        // Headers I care about
        //String h_host;

        // Fixed number of lines to prevent DOS due to ill crafted get request.
        String header, value;
        int limit = HTTPWS_MAX_HEADERS;
        Serial.println( F("HTTP Headers") );
        while( client->nextHeader( header, value ) && limit > 0 ){
            Serial.print( F("\t") );
            Serial.print( header );
            Serial.print( F(":") );
            Serial.println( value );
            yield();
            //LOGI( header, F( ":" ), value );
            limit--;

            /*if( !header.compareTo( F( "host" ) ) ){
                h_host = value;
                continue;
            }*/
        }

        /* http GET request
         * =========================
         */
        const String &resource = client->getResource();
        if( SD.cardErrorCode() || !SD.vwd()->isOpen() ){
            return client->httpResponse( 500, F( "SD Card Error" ) );
            // TODO Add SD Error Code and data to headers.
        }

        if( !SD.exists( resource.c_str() ) ){

            return client->httpResponse( 404, F( "Not Found" ) );
        }

        file.open( resource.c_str(), O_RDONLY );
        if( !file.isOpen() ){
            return client->httpResponse( 500, F( "Unable to open resource" ) );
        }

        if( file.isFile() ){
            // Determine the Content-Type for correct interpretation of the data by the client
            char filename[128];
            String content_type;
            file.getName( filename, 128 );
            if( String( filename ).endsWith( ".html" ) ){
                content_type = F( "text/html; charset=utf-8" );
            } else if( String( filename ).endsWith( ".js" ) ){
                content_type = F( "text/javascript; charset=utf-8" );
            } else if( String( filename ).endsWith( ".css" ) ){
                content_type = F( "text/css; charset=utf-8" );
            } else{
                content_type = F( "text/plain; charset=utf-8" );
            }


            {
                Pair< String, String > headers[]{
                        {F("Access-Control-Allow-Origin"), F("*")},
                        {F( "Content-Length" ), String( file.size(), DEC ) },
                        {F( "Content-Type" ),   content_type}};
                client->httpResponse( 200, {3, headers}, F( "OK" ) );
            }
            sdfatAction = &httpws::HandleSdFat::httpGETFile;
            return OpCode::CONTINUE;
        }

        if( file.isDir() ){
            sdfatAction = &HandleSdFat::httpGETDir;
            return OpCode::CONTINUE;
        }

        //LOGW( F( "resource is unknown type" ) );
        return client->httpResponse( 400, F( "Bad Request" ) );
    }

    OpCode HandleSdFat::httpPOST(){
        return client->httpResponse( 501, F( "Not Implemented" ) );
    }

    OpCode HandleSdFat::httpDELETE(){
        // Fixed number of header lines to prevent DOS due to ill crafted request.
        String header, value;
        int limit = HTTPWS_MAX_HEADERS;
        while( client->nextHeader( header, value ) && limit > 0 ){
            limit--;
        }

        if( SD.cardErrorCode() || !SD.vwd()->isOpen() ){
            return client->httpResponse( 500, F( "SD Card not enabled" ) );
            //TODO add error code and data into headers
        }
        if( !SD.exists( client->getResource().c_str() ) ){
            return client->httpResponse( 404, F( "Not Found" ) );
        }

        file.open( client->getResource().c_str(), O_READ );
        if( !file.isOpen() ){
            file.close();
            return client->httpResponse( 500, F( "Unable to open resource" ) );
        }

        if( file.isReadOnly() ){
            file.close();
            return client->httpResponse( 403, client->getResource() + F( " is readonly" ) );
        }

        if( file.isDir() ){
            //TODO consider putting in header guards for deleting whole directories
            if( !file.rmRfStar() ){
                file.close();
                return client->httpResponse( 500, F( "Failed to delete directory" ) );
            }
            //TODO delete recursively.
        }
        file.close();

        if( !SD.remove(client->getResource().c_str()) ){
            return client->httpResponse( 500, String( F( "Failed to delete resource" ) ) );
        }
        SD.vwd()->sync();

        return client->httpResponse( 204, String( F( "Successfully deleted resource" ) ) );
    }

    OpCode HandleSdFat::httpGETFile(){
        //Check state
        if( !client->connected() ){
            file.close();
//        LOGE( F( "Client Not Connected" ) );
            return OpCode::ERROR;
        }

        if( !file.isOpen() ){
//        LOGE( F( "File is not open for reading" ) );
            return OpCode::ERROR;
        }

        if( file.available() == 0 ){
            file.close();
//        LOGI( F( "No more data available" ) );
            return OpCode::CLOSE;
        }

        //Allocate Memory for reading file, its faster to do it in chunks than byte
        //by byte. however it takes up ram :(

        //NOTES: There are a number of possible bottlenecks for how to do this,
        //like sd read size, network write size, ram cache size etc, I performed
        //some comparisons in the past to know how to tune the size of the buffer
        //and found the best value to be 2048, however that is a large chunk for
        //these types of devices.

        long mem_size = min( 2048, freeMemory() - 1024 ); //leave at least 1024 bytes available
        mem_size -= mem_size % 256; //round down to closest multiple of 256;

        if( mem_size <= 0 ){
            file.close();
//        LOGE( F( "Out of memory when processing GET" ), client->getResource() );
            return OpCode::ERROR;
        }

//    LOGO( F( "Allocating ram for transfer:" ), mem_size );
        uint8_t mem[mem_size];

        int length = file.read( mem, mem_size );
        if( length == -1 ){
            file.close();
            delay( 1 );
//        LOGE( F( "Reading from SD Card:" ), client->getResource() );
            return OpCode::ERROR;
        }

        client->write( mem, length );
        delay( 1 );

//    LOGO( F( "Transferred" ), file.position(), F( "of" ), String( file.size() ) );
        return OpCode::NONE;
    }

//TODO add date to httpGETDir output, and tabularise
    OpCode HandleSdFat::httpGETDir(){
        if( !client->connected() ){
            file.close();
            Serial.println("Client is not connected");
            return OpCode::ERROR;
        }

        Pair<String,String> headers[]{
                {F("Access-Control-Allow-Origin"), F("*")},
                {F("Content-Type"),F("text/html")}
            };
        client->httpResponse( 200, {2,headers},F( "OK" ) );

        // TODO find some way to estimate th length of the file
        // Will require to limit its generation to fit within hardware limits.

        String resource = client->getResource(); // make mutable copy of the resource string
        client->println( F( R"html(<!DOCTYPE html>
<head>
<title>Directory List</title>
</head>
<body>
<h1>Directory Listing</h1>)html" ) );

        client->print( F( "<p>" ) );
        client->print( resource );
        client->print( F( "</p>\n" ) );

        //build up our directory listing string.
        String body;
        File sub_file;
        if( resource.endsWith( "/" ) )resource.setCharAt( resource.length() - 1, ' ' );
        resource.trim();
        while( sub_file.openNext( &file, O_READ ) ){
            char buffer[256];
            body += F( "<a href=\"" );
            if( resource != "/" ){
                body += resource;
                body += "/";
            }
            sub_file.getName( buffer, 256 );
            body += buffer;
            body += F( "\">" );
            body += resource;
            body += F( "/" );
            body += buffer;
            if( sub_file.isDir() ) body += F( "/" );
            body += F( "</a> Size:" );
            body += String( sub_file.size() );
            body += F( "<br>\n" );
            sub_file.close(); //TODO Test whether this is needed.
        }
        client->print( body );
        client->print( F( "</body>\r\n" ) );
        return OpCode::CLOSE;
    }

    OpCode HandleSdFat::httpPUT(){
/* SdFat supports Long File Names. Long names in SdFat are limited to 7-bit
 * ASCII characters in the range 0X20 - 0XFE The following are reserved
 * characters:*/
        const String reserved_chars( F( "<>:\"|?*" ) );
        for( const auto c : client->getResource() ){
            if( reserved_chars.indexOf( c ) != -1 ){
                return client->httpResponse( 400, F( "resource contains a reserved character: <>:\"|?*" ) );
            }
            if( c < 0X20 || c > 0XFE ){
                return client->httpResponse( 400, F( "resource contains characters outside of ascii range 0X20-0XFE" ) );
            }
            //TODO 414 URI Too Long
        }

// HTTP Payload Scemantics is useful here
// https://tools.ietf.org/html/rfc7231#section-3.3

        //Headers values
        h_content_length = 0;
        bool h_continue = false;

        // Fixed number of lines to prevent DOS due to ill crafted get request.
        String header, value;
        int limit = HTTPWS_MAX_HEADERS;
        while( client->nextHeader( header, value ) && limit > 0 ){
            limit--;
            //Check headers
            if( !header.compareTo( F( "content-length" ) ) ){
                h_content_length = atol( value.c_str() );
                continue;
            } else if( !header.compareTo( F( "expect" ) ) ){
                if( !value.compareTo( F( "100-continue" ) ) ){
                    h_continue = true;
                }
            } else if( !header.compareTo( F( "content-range" ) ) ){
                return client->httpResponse( 400, F( "PUT Request includes 'Content-Range'" ) );
            }
        }

        if( SD.cardErrorCode() || !SD.vwd()->isOpen() ){
            return client->httpResponse( 500, F( "SD Card Error" ) );
            //TODO put error code and data into headers
        }


        if( SD.exists( client->getResource().c_str() ) ){
            status = 204; //No Content
        } else{
            status = 201; //Created
        }

        //FIXME Check to see if the file is already open by another connection

        //Build subtre if it doesnt exist
        // TODO no need to build subtree if the file exists
        String resource = client->getResource(); //make a copy of resource
        int delim = resource.indexOf( '/' );
        String folder = "/";
        while( delim != -1 ){
            folder = resource.substring( 0, delim + 1 );

            if( !SD.exists( folder.c_str() ) ){
                if( !SD.mkdir( folder.c_str() ) ){
                    return client->httpResponse( 500, F( "Internal Server Error" ) );
                }
            }
            if( !SD.chdir( folder.c_str(), true ) ){
                return client->httpResponse( 500, F( "Cant change to folder" ) );
            }

            resource = resource.substring( delim + 1, resource.length() );
            delim = resource.indexOf( '/' );
        }

        file.open( resource.c_str(), O_WRITE | O_TRUNC | O_CREAT | O_SYNC );
        if( !file.isOpen() ){
            file.close();
            return client->httpResponse( 500, F( "Error opening resource for writing" ) );
        }

        //send approval for the client to send the data
        if( h_continue ){
            client->httpResponse( 100, F( "Continue" ) );
        }

        sdfatAction = &HandleSdFat::httpPUTCont;
        return OpCode::CONTINUE;
    }

    OpCode HandleSdFat::httpPUTCont(){
        if( !client->connected() ){
            file.close();
            return OpCode::ERROR;
        }
        if( !file.isOpen() ){
            file.close();
            return client->httpResponse( 500, F( "Error opening resource for writing" ) );
        }

        int mem_size = min( HTTPWS_MAX_BUFFER, freeMemory() );
        uint8_t mem[mem_size];

        if( !client->available() ){
            return OpCode::NONE;
        }

        int length = client->read( mem, mem_size );
        if( length == -1 ){
            file.close();
            return client->httpResponse( 500, F( "Error when reading from client " ) );
        }

        if( !file.isOpen() ){
            file.close();
            return client->httpResponse( 500, F( "File is not open for writing" ) );
        }
        int written = file.write( mem, length );
        file.sync();
        if( written == -1 ){
            file.close();
            return client->httpResponse( 500, F( "Error when writing to file" ) );
        }

        if( file.position() >= h_content_length ){
            file.close();
            /* send a 201 (Created) response.
                If the target resource does not have a current representation and the
                PUT successfully creates one, then the origin server MUST inform the
                user agent by sending a 201 (Created) response.
                If the target resource does have a current representation and that
                representation is successfully modified in accordance with the state of the
                enclosed representation, then the origin server MUST send either a 200 (OK)
                or a 204 (No Content) response to indicate successful completion of the
                request.*/
            if( status == 201)
                return client->httpResponse( 201, F( "Created" ) );
            else
                return client->httpResponse( 204, F( "No Content" ) );
        }
        return OpCode::NONE;
    }
}
