//
// Created by enetheru on 10/11/20.
//

#ifndef ARDUINO_TEMPLATES_HPP
#define ARDUINO_TEMPLATES_HPP

/* These templates are bare bones and sparse, not at all what i would consider good practice. */

template< typename T >
class Array{
    T *d;
    unsigned int s;
public:
    Array() : s(0), d(nullptr) {}
    Array( unsigned int size, T *data ): s(size), d(data) {};
    int size() const { return s; }
    T &operator[]( unsigned int i) { return d[i]; }
    const T &operator[]( unsigned int i) const { return d[i]; }


    typedef T * iterator;
    iterator begin() { return &d[0]; }
    iterator end() { return &d[s]; }

    typedef const T * const_iterator;
    const_iterator begin() const { return &d[0]; }
    const_iterator end() const { return &d[s]; }
};

template<typename T, typename U>
struct Pair{
    T first;
    U second;
};

template< typename T, typename U >
class Map : public Array< Pair< T, U > >{
public:
    Map() : Array<Pair< T, U > >() {}
    Map( unsigned int size, Pair< T, U > *data ) :
            Array< Pair< T, U > >( size, data ) { }
};

template<typename T>
class CircleArray: public Array<T> {
    T *current;
public:
    CircleArray() : Array<T>() {}
    CircleArray( unsigned int size, T *data ) : Array<T>( size, data){
        current = this->begin();
    }

    typedef T * iterator;
    iterator next() {
        current++;
        if( current == this->end() ){
            current = this->begin();
        }
        return current;
    }

    typedef const T * const_iterator;
    const_iterator next() const {
        current++;
        if( current == this->end() ){
            current = this->begin();
        }
        return current;
    }
};
#endif //ARDUINO_TEMPLATES_HPP
