//
// Created by enetheru on 16/4/20.
//

#ifndef HTTPWS_HANDLE_SDFAT_H
#define HTTPWS_HANDLE_SDFAT_H
#include <SdFat.h>

namespace httpws {

class HandleSdFat : public Handle{
    SdFat SD;
    File file;
    int HTTPWS_MAX_HEADERS = 100;
    uint32_t h_content_length{};
    int HTTPWS_MAX_BUFFER = 2048;
    unsigned int status = 0;

    using ActionFunc = httpws::OpCode (HandleSdFat::*)();
    ActionFunc sdfatAction{};

public:
    explicit HandleSdFat(Client *client);
    static Handle *factory(Client *client);
    ~HandleSdFat() override = default;

    OpCode resume() override;

    // Read overrides
    size_t read( uint8_t &d ) override{ return 0;};

    // Write overrides
    size_t write( uint8_t d ) override{ return 0;};
    size_t write( const uint8_t *data, size_t length) override { return 0;};

    bool error();

private:
    OpCode httpGET();
    OpCode httpPOST();
    OpCode httpPUT();
    OpCode httpDELETE();

    OpCode httpGETFile();
    OpCode httpGETDir();
    OpCode httpPUTCont();
};

}
#endif //HTTPWS_HANDLE_SDFAT_H
