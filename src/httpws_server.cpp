//
// Created by enetheru on 17/4/20.
//
#include "httpws.h"

namespace httpws{

    void Server::assignClients( CircleArray<Client> clients ){
        this->clients = clients;
        for( int i = 0; i < clients.size(); ++i ){
             this->clients[ i ] = Client( i ); //re-initialise clients with correct port numbers
        }
    }

    void Server::assignBindings( Map<String, Binding> handles ){
        this->bindings = handles;
    }

    OpCode Server::read(){

        available();
        // Without this available() call, the server stops working after the first client is closed.
        // Reasoning written in Diary.md 2020-11-12

        // server.available() doesnt provide a robust enough loop and would favour the first connection
        // looping through a circular array keeps things moving forward
        // FIXME instead of looping for a set number, perhaps loop for a set time period
        for( unsigned int loop = 0; loop < clients.size(); ++loop ){
            Client &client = *clients.next();
            OpCode opcode = client.act();

            switch( opcode ){
                case OpCode::NONE:
                    continue;
                case OpCode::AVAILABLE:
                    return client.react();
                case OpCode::NEW:
                    //Loop through resource bindings for a match.
                    // matching is greedy '/' will match '/*' so more specific handlers need to come first.
                    for( const auto &binding : bindings ){
                        if( client.getResource().startsWith( binding.first ) ){
                            Serial.print(F("Handler Found for: "));
                            Serial.println( client.getResource() );

                            Handle *handle = binding.second.factory( &client );
                            if(! handle ) {
                                close( client );
                                return OpCode::ERROR;
                            }
                            client.setHandler( handle );
                            client.setCallback( binding.second.callback );
                            return opcode;
                        }
                    }
                    return client.httpResponse( 404, F( "Not Found" ) );
                case OpCode::CONTINUE:
                    return opcode;
                case OpCode::CLOSE:
                    //TODO leave port open for subsequent connections and close on a timer.
                    close( client );
                case OpCode::ERROR:
                    Serial.println(F("Client error Closing connection"));
                    close( client );
                    return opcode;
            }
        }
        return OpCode::NONE;
    }

    void Server::close( Client &client ){
        int id = client.getSocketNumber();
        client.close();
        clients[id] = httpws::Client(id);
    }

} // end namespace httpws