/*
 * http/websocket server
 */
#ifndef ARDUINO_HTTPWS_H
#define ARDUINO_HTTPWS_H

/*
* TODO if i implement the print and write will that enable streaming?
* TODO review httpError and httpOK calls for missing required headers
        * TODO Write test cases for large transfers, HTTP {GET,PUT,POST}, and
        * WEBSOCKET {SEND,RECV} due to the limited bandwidth the sending and
        * receiving of payloads needs to be interleaved to prevent blocking of the
        * mainloop
* TODO replace EthernetServer by copying it and modifying it as necessary, its not very helpful.
* the reason is that when looking into the code it performs needless copying and generation of memory data all the time.
* any available() call creates an ethernetClient object, which to me is mind boggling. but i guess its defensive against idiots
*
*/

#include <Print.h>
#include <Client.h>
#include <Ethernet.h>
#include <EthernetServer.h>
#include <ArduinoJson.h>
#include "templates.hpp"

namespace httpws{

    enum struct OpCode{
        NONE,
        AVAILABLE,
        NEW,
        CONTINUE,
        CLOSE,
        ERROR,
    };

    enum struct Method{
        GET,
        HEAD,
        POST,
        PUT,
        DELETE,
        CONNECT,
        OPTIONS,
        TRACE
    };

    class Client;

    /* Resource Handler Class
     * ======================
     */
    class Handle : public Print{
    protected:
        Client *client{};
    public:
        virtual OpCode resume() = 0;

        virtual ~Handle() = default;

        // Print override
        virtual size_t read( uint8_t & ) = 0;

        // Text write
        size_t write( uint8_t value ) override = 0;
        size_t write( const uint8_t *data, size_t length ) override = 0;
        virtual void write( const String &text ){};
        virtual void write( const JsonDocument &doc ){};
    };

    /* HTTP/Websocket Client Class
     * ===========================
     */
    class Client : public EthernetClient{
        Method method{};
        String resource{};
        Handle *handle{};
        void (*reaction)( Client * ){};

    public:
        Client() = default;
        explicit Client( const EthernetClient &client )
                : EthernetClient( client.getSocketNumber() ){}

        void close();

        //setters
        void setHandler( Handle * );
        void setCallback( void(*)( Client * ) );

        //getters
        Handle *getHandle(){ return handle; };
        const String &getResource(){ return resource; };
        const Method &getMethod() { return method; }

        // functions
        OpCode httpListen();
        OpCode act();
        OpCode react();
        void printHeader( const Pair<String, String> &header );
        bool nextHeader( String &header, String &value );
        OpCode httpResponse( int code, const String &message);
        OpCode httpResponse( int code, Map<String,String> headers, const String &message );
    };

    /* Server class declaration
     * ========================
     */
    class Server : public EthernetServer{
    public:
        struct Binding{
            using Factory = Handle *( * )( Client * );
            using Callback = void ( * )( Client * );

            Binding( Factory func, Callback cb )
                    : factory( func ),
                      callback( cb ){};

            Factory factory;
            Callback callback;
        };

    private:
        // data for this circular array is declared externally
        CircleArray<Client> clients;

        // data for this map is declared externally
        Map<String, Binding> bindings{};

    public:
        // Construction
        explicit Server( int port ) : EthernetServer( port ){}

        void assignClients( CircleArray<Client> clients );
        void assignBindings( Map<String, Binding> handles );

        OpCode read();

        void close( Client &client );
    };


} // end namespace httpws


#endif// ARDUINO_HTTPWS_H
