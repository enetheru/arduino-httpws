//
// Created by enetheru on 19/4/20.
//

#include "httpws.h"
#include "httpws_handle_websocket.h"
#include "httpws_ws_handshake.h"


namespace httpws{

    Handle *HandleWebsocket::factory( Client *client ){
        if( client->getMethod() != Method::GET ) return nullptr;

        auto *handle = new HandleWebsocket( client );
        handle->wsAction = &HandleWebsocket::httpGET;

        return handle;
    }

OpCode HandleWebsocket::resume(){
    return (this->*wsAction)();
}

OpCode HandleWebsocket::httpGET(){
    Serial.println(F("websocket.httpGET()"));
/* https://tools.ietf.org/html/rfc7231#section-5
5.  Request Header Fields

   A client sends request header fields to provide more information
   about the request context, make the request conditional based on the
   target resource state, suggest preferred formats for the response,
   supply authentication credentials, or modify the expected request
   processing.  These fields act as request modifiers, similar to the
   parameters on a programming language method invocation.
 */

    // WebSocket Headers I care about
    String h_host,
            h_upgrade,
            h_connection,
            h_sec_websocket_key;
    long h_sec_websocket_version = 0;

    // Fixed number of lines to prevent DOS due to ill crafted get request.
    String header, value;
    int limit = 100;
    Serial.println( F("HTTP Headers") );
    while( client->nextHeader( header, value ) && limit > 0 ){
        Serial.print( F("\t") );
        Serial.print( header );
        Serial.print( F(":") );
        Serial.println( value );

        limit--;

        if( !header.compareTo( F( "host" ) ) ){
            h_host = value;
            continue;
        }
        if( !header.compareTo( F( "upgrade" ) ) ){
            h_upgrade = value;
            h_upgrade.toLowerCase();
            continue;
        }
        if( !header.compareTo( F( "connection" ) ) ){
            h_connection = value;
            h_connection.toLowerCase();
            continue;
        }
        if( !header.compareTo( F( "sec-websocket-key" ) ) ){
            h_sec_websocket_key = value;
            continue;
        }
        if( !header.compareTo( F( "sec-websocket-version" ) ) ){
            h_sec_websocket_version = value.toInt();
            continue;
        }
    }

    /*
     * WebSocket Upgrade Request
     * =========================
     */
/* https://tools.ietf.org/html/rfc6455#section-4.2.1
4.2.1.  Reading the Client's Opening Handshake

   When a client starts a WebSocket connection, it sends its part of the
   opening handshake.  The server must parse at least part of this
   handshake in order to obtain the necessary information to generate
   the server part of the handshake.

   The client's opening handshake consists of the following parts.  If
   the server, while reading the handshake, finds that the client did
   not send a handshake that matches the description below (note that as
   per [RFC2616], the order of the header fields is not important),
   including but not limited to any violations of the ABNF grammar
   specified for the components of the handshake, the server MUST stop
   processing the client's handshake and return an HTTP response with an
   appropriate error code (such as 400 Bad Request).

   1.   An HTTP/1.1 or higher GET request, including a "Request-URI"
        [RFC2616] that should be interpreted as a /resource name/
        defined in Section 3 (or an absolute HTTP/HTTPS URI containing
        the /resource name/).

   2.   A |Host| header field containing the server's authority.

   3.   An |Upgrade| header field containing the value "websocket",
        treated as an ASCII case-insensitive value.

   4.   A |Connection| header field that includes the token "Upgrade",
        treated as an ASCII case-insensitive value.

   5.   A |Sec-WebSocket-Key| header field with a base64-encoded (see
        Section 4 of [RFC4648]) value that, when decoded, is 16 bytes in
        length.

   6.   A |Sec-WebSocket-Version| header field, with a value of 13.
 */
    if(! h_connection.equals( F( "upgrade" ) ) ){
        return client->httpResponse( 400,
                F( "Bad Request" ) );
    }

    if(! h_upgrade.equals( F( "websocket" ) ) ){
        return client->httpResponse( 400,
                F( "Bad Upgrade Request - Unknown value" ) );
    }

    if( h_host.length() == 0 ){
        return client->httpResponse( 400,
                F( "Bad Websocket Request - missing |host|" ) );
    }

    if( h_sec_websocket_key.length() == 0 ){
        return client->httpResponse( 400,
                F( "Bad Websocket Request - missing |sec-websocket-key|" ) );
    }

    if( h_sec_websocket_version != 13 ){
        return client->httpResponse( 400,
                F( "Bad Websocket Request - invalid |sec-websocket-version|" ) );
    }

/* https://tools.ietf.org/html/rfc6455#section-4.2.2
 WebSocket
   5.  If the server chooses to accept the incoming connection, it MUST
       reply with a valid HTTP response indicating the following.

       1.  A Status-Line with a 101 response code as per RFC 2616
           [RFC2616].  Such a response could look like "HTTP/1.1 101
           Switching Protocols".
       2.  An |Upgrade| header field with value "websocket" as per RFC
           2616 [RFC2616].
       3.  A |Connection| header field with value "Upgrade".
       4.  A |Sec-WebSocket-Accept| header field.  The value of this
           header field is constructed by concatenating /key/, defined
           above in step 4 in Section 4.2.2
 */
    {
        char output[29]{};
        WebSocketHandshake::generate( h_sec_websocket_key.begin(), output );
        Serial.println(F("WebSocket requested accepted, sending upgrade response"));
        Pair<String,String> headers[]{
            {F("Access-Control-Allow-Origin"), F("*")},
            {F("Upgrade"),F("websocket")},
            {F("Connection"),F("Upgrade")},
            {F("Sec-WebSocket-Accept"), output }
            };
        client->httpResponse( 101, {4,headers}, F( "Switching Protocols" ) );
    }
/* This completes the server's handshake.  If the server finishes these
   steps without aborting the WebSocket handshake, the server considers
   the WebSocket connection to be established and that the WebSocket
   connection is in the OPEN state. At this point, the server may begin
   sending (and receiving) data.
 */
    wsAction = &HandleWebsocket::listen;
    return OpCode::CONTINUE;
}

/* Internal Functions */
OpCode HandleWebsocket::listen(){
    if( !client->available() ) return OpCode::NONE;

/* WebSocket
   Base Framing Protocol
      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-------+-+-------------+-------------------------------+
     |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
     |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
     |N|V|V|V|       |S|             |   (if payload len==126/127)   |
     | |1|2|3|       |K|             |                               |
     +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
     |     Extended payload length continued, if payload len == 127  |
     + - - - - - - - - - - - - - - - +-------------------------------+
     |                               |Masking-key, if MASK set to 1  |
     +-------------------------------+-------------------------------+
     | Masking-key (continued)       |          Payload Data         |
     +-------------------------------- - - - - - - - - - - - - - - - +
     :                     Payload Data continued ...                :
     + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
     |                     Payload Data continued ...                |
     +---------------------------------------------------------------+
*/
    Serial.println( F( "--New WebSocket Frame" ) );
    msgPos = 0;
    uint8_t opcodes = client->read(); //First byte
    Serial.print( F("1st byte: ") );
    Serial.println( String( opcodes, BIN ) );

    //OpCodes
//    bool fin = opcodes & 0b10000000u;

    uint8_t length = client->read(); //Second byte
    Serial.print( F("2nd byte: ") );
    Serial.println( String( length, BIN ) );

    // frames from web clients are always masked.
    bool is_mask = length & 0b10000000u;
    //LOGO( F( "MASK: " ), ismask ? F( "true" ) : F( "false" ) );

    //Frame Length
    msgLength = length & 0b01111111u;
    Serial.print( F("length: ") );
    Serial.println( String(msgLength,DEC) );

    if( msgLength > 126 ){
        // FIXME deal with frames with 16bit and 64bit lengths
        return close();
    }

    //Read the mask
    if( is_mask ){ //this should always be true
        msgMask[ 0 ] = client->read();
        msgMask[ 1 ] = client->read();
        msgMask[ 2 ] = client->read();
        msgMask[ 3 ] = client->read();
    }

    switch( opcodes & 0b00001111u ){
        case WS_OP_FIN:
            Serial.println(F("WS_OP_FIN"));
            break;
        case WS_OP_CONT:  // 0b0000, %x0 denotes a continuation frame
            Serial.println(F("WS_OP_CONT"));
            //TODO handle fragmented data
            return OpCode::ERROR;
        case WS_OP_TEXT:  // 0b0001, %x1 denotes a text frame
            Serial.println(F("WS_OP_TEXT"));
            wsAction = &HandleWebsocket::wait;
            //FIXME return OC( WS_TEXT );
            return OpCode::CONTINUE;
        case WS_OP_BIN:   // 0b0010, %x2 denotes a binary frame
            Serial.println(F("WS_OP_BIN"));
            wsAction = &HandleWebsocket::wait;
            //FIXME return OC( WS_BIN );
            return OpCode::ERROR;
        case WS_OP_CLOSE: // 0b1000, %x8 denotes a connection close
            Serial.println(F("WS_OP_CLOSE"));
            return close();
        case WS_OP_PING:  // 0b1001, %x9 denotes a ping
            Serial.println(F("WS_OP_PING"));
            pong();
            return OpCode::ERROR;
        case WS_OP_PONG:  // 0b1010, %xA denotes a pong
            Serial.println(F("WS_OP_PONG"));
            //TODO Report the latency, set counter for wsListen to keep track of.
            return OpCode::ERROR;
    }
    Serial.println(F("Websockt Opcode Error"));
    return OpCode::ERROR;
}

OpCode HandleWebsocket::wait(){
    Serial.println( F("wsWait()") );
    Serial.print( F("ws_msgPos: ") );
    Serial.println( String( msgPos, DEC ) );
    Serial.print( F("ws_msgLength: ") );
    Serial.println( String( msgLength, DEC ) );
    if( msgPos >= msgLength ){
        wsAction = &HandleWebsocket::listen;
        return OpCode::CONTINUE;
    }
    return OpCode::AVAILABLE;
}

size_t HandleWebsocket::read( uint8_t &value ){
    if( msgPos < msgLength ){
        if( !client->available() ){
            //LOGE( F( "premature lack of data." ) );
            return 0;
        }
        byte masked = client->read();
        value = msgMask[ (msgPos) % 4 ] ^ masked;
        ++msgPos;
        return 1;
    }
    return 0;
}

unsigned long HandleWebsocket::read( uint8_t *data, unsigned long &length ){
    unsigned int read_pos = 0;
    while( msgPos < msgLength && read_pos < length ){
        if( !client->available() ){
            //LOGE( F( "premature lack of data." ) );
            length = read_pos;
            return 0;
        }
        byte masked = client->read();
        data[ read_pos ] = msgMask[ msgPos % 4 ] ^ masked;
        ++msgPos;
        ++read_pos;
    }
    length = read_pos;
    return length; //WS_RECV;
}

OpCode HandleWebsocket::close(){
    client->write( WS_OP_FIN | WS_OP_CLOSE );
    client->write( uint8_t( 0 ) ); // mask = 0, and length 0;

    wsAction = nullptr;
    return OpCode::CLOSE;
}

OpCode HandleWebsocket::ping(){
//5.5.2.  Ping
//
//   The Ping frame contains an opcode of 0x9.
    uint8_t opcode = 0b10001001;
    uint8_t length = 0;
//
//   A Ping frame MAY include "Application data".
//
//   An endpoint MAY send a Ping frame any time after the connection is
//   established and before the connection is closed.
//   TODO if connection()?
//
//   NOTE: A Ping frame may serve either as a keepalive or as a means to
//   verify that the remote endpoint is still responsive.
    client->write( opcode );
    client->write( length );
    return OpCode::CONTINUE; //WS_PING;
}

OpCode HandleWebsocket::pong(){
//5.5.3.  Pong
//
//   The Pong frame contains an opcode of 0xA.
    //uint8_t opcode = 0b10001010;
//
//   Section 5.5.2 details requirements that apply to both Ping and Pong
//   frames.
//
//   A Pong frame sent in response to a Ping frame must have identical
//   "Application data" as found in the message body of the Ping frame
//   being replied to.
//
//   If an endpoint receives a Ping frame and has not yet sent Pong
//   frame(s) in response to previous Ping frame(s), the endpoint MAY
//   elect to send a Pong frame for only the most recently processed Ping
//   frame.
//
//   A Pong frame MAY be sent unsolicited.  This serves as a
//   unidirectional heartbeat.  A response to an unsolicited Pong frame is
//   not expected.
    return OpCode::ERROR;
}


size_t HandleWebsocket::write( const uint8_t *data,
                               unsigned int length, //FIXME make the length a 64bit unsigned integer
                              wsOpCode type ){
/*
Fette & Melnikov             Standards Track                   [Page 38]

RFC 6455                 The WebSocket Protocol            December 2011

 6.  Sending and Receiving Data

6.1.  Sending Data

   To _Send a WebSocket Message_ comprising of /data/ over a WebSocket
   connection, an endpoint MUST perform the following steps.

   1.  The endpoint MUST ensure the WebSocket connection is in the OPEN
       state (cf. Sections 4.1 and 4.2.2.)  If at any point the state of
       the WebSocket connection changes, the endpoint MUST abort the
       following steps.

   2.  An endpoint MUST encapsulate the /data/ in a WebSocket frame as
       defined in Section 5.2.  If the data to be sent is large or if
       the data is not available in its entirety at the point the
       endpoint wishes to begin sending the data, the endpoint MAY
       alternately encapsulate the data in a series of frames as defined
       in Section 5.4.

   3.  The opcode (frame-opcode) of the first frame containing the data
       MUST be set to the appropriate value from Section 5.2 for data
       that is to be interpreted by the recipient as text or binary
       data.

   4.  The FIN bit (frame-fin) of the last frame containing the data
       MUST be set to 1 as defined in Section 5.2.

   5.  If the data is being sent by the client, the frame(s) MUST be
       masked as defined in Section 5.3.

   6.  If any extensions (Section 9) have been negotiated for the
       WebSocket connection, additional considerations may apply as per
       the definition of those extensions.

   7.  The frame(s) that have been formed MUST be transmitted over the
      underlying network connection.
 */
    //LOGO( F( "sending" ), length, F( "bytes" ) );
    if( length < 126 ){
        client->write( WS_OP_FIN | type );
        client->write( length );
        client->write( data, length );
    }else if( length < 65535 ){
        uint16_u length16{};
        length16.value = length;
        client->write( WS_OP_FIN | type );
        client->write( 126 );
        client->write( length16.bytes[ 1 ] );
        client->write( length16.bytes[ 0 ] );
        client->write( data, length );
    }else{
        //FIXME temporary fix for large packets, need to eventualyl split this
        //up into pieces and work with it
        //LOGE( F( "frame length of over 65536 is unsupported" ) );
    }
    return 0;
}

size_t HandleWebsocket::write( uint8_t value ){
    return write( (const uint8_t *)&value, 1, WS_OP_BIN );
}

size_t HandleWebsocket::write( const uint8_t *data,
                               size_t length ){
    return write( data, length, WS_OP_BIN );
}

void HandleWebsocket::write( const String &text ){
    write( (const uint8_t *)text.begin(), text.length(), WS_OP_TEXT );
}

// Less memory intensive way to send json data to websocket connections.
void HandleWebsocket::write( const JsonDocument &doc ){

    unsigned long length = measureJson( doc );
    if( length > 65535 ){
        //LOGE( F( "frame length of over 65536 is unsupported" ) );
        return;
    }

    //opcode
    client->write( WS_OP_FIN | WS_OP_TEXT );

    //length
    if( length < 126 ){
        client->write( length );
    }else if( length < 65535 ){
        uint16_u length16{};
        length16.value = length;
        client->write( 126 );
        client->write( length16.bytes[ 1 ] );
        client->write( length16.bytes[ 0 ] );
    }

    //content
    serializeJson( doc, *client );
}
}