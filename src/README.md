arduino-httpws
==============
http and websocket server for the arduino environment

## High Level TODO

* [ ] Make serving data from the SD card the same process as a registered
   resource handle. this will make the SDFat lib optional.
* [ ] Add Examples
* [ ] remove dependency on https://gitlab.com/enetheru/arduino-lumberjack 
* [ ] split Client into http and websocket clients. 


## HTTP
The http server is minimalistic with no security, its primary purpose is to
fulfill a subset of http requests to serve static web pages, update contents of
SD card, and provide upgrades to websocket connections for realtime
communications.

At the moment all the files on the SD card are read/write, so there is no user
based permission scheme or security measures, its very simplistic just to have
a mechanism for doing stuff via the web browser.

HTTP Requests planned:

* GET
* HEAD
* POST
* PUT
* DELETE
* OPTIONS

### GET
Get is implemented and supports:

* requesting files from the SD card
* Get file listing from  a folder request
* Upgrade to websocket
* run a function with a registered resource handle

### HEAD
Not yet implemented

### POST
Not yet implemented

### PUT
uploads and overwrites files creating folders as necessary

### DELETE
delete's files, but not folders, and cannot recursively delete yet,

### OPTIONS
Not yet implemented


## WebSockets
The websocket connection provides realtime communicatin with the system by
routing any new websocket frames to a registered user created handler function
for action.

The implementation works fine so far, even though its rudimentary and missing a
lot of functionality

### TODO
* [ ] PING/PONG responses
* [ ] Multi frame split messages
* [ ] messages larger than 1024 bytes

