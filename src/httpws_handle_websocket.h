//
// Created by enetheru on 19/4/20.
//

#ifndef ARDUINO_HTTPWS_HTTPWSHANDLEWEBSOCKET_H
#define ARDUINO_HTTPWS_HTTPWSHANDLEWEBSOCKET_H

#include <ArduinoJson.h>
#include "httpws.h"

namespace httpws{

class HandleWebsocket : public Handle {
    union uint16_u{
        uint16_t value;
        uint8_t bytes[2];
    };

    union uint64_u{
        uint8_t bytes[8];
        uint64_t value;
    };


    enum wsOpCode : uint8_t{
        /* Opcode:  4 bits
            Defines the interpretation of the "Payload data".  If an unknown
            opcode is received, the receiving endpoint MUST _Fail the
            WebSocket Connection_.  The following values are defined. */
        WS_OP_FIN = 0b10000000u,
        WS_OP_CONT = 0b00000000u,  // 0b0000, %x0 denotes a continuation frame
        WS_OP_TEXT = 0b00000001u,  // 0b0001, %x1 denotes a text frame
        WS_OP_BIN = 0b00000010u,  // 0b0010, %x2 denotes a binary frame
        // %x3-7 are reserved for further non-control frames
        WS_OP_CLOSE = 0b00001000u,  // 0b1000, %x8 denotes a connection close
        WS_OP_PING = 0b00001001u,  // 0b1001, %x9 denotes a ping
        WS_OP_PONG = 0b00001010u   // 0b1010, %xA denotes a pong
        // %xB-F are reserved for further control frames
    };

    //Websocket State Information
    unsigned long msgPos = 0, msgLength = 0;
    uint8_t msgMask[4]{};

    using ActionFunc = httpws::OpCode (httpws::HandleWebsocket::*)();
    ActionFunc wsAction{};

public:
    explicit HandleWebsocket(Client *client) { this->client = client; }
    static Handle *factory(Client *client);;
    ~HandleWebsocket() override = default;

    OpCode resume() override;

    // Read
    size_t read( uint8_t &value ) override;
    unsigned long read( uint8_t *data, unsigned long &length );

    // Write
    size_t write( uint8_t value ) override;
    size_t write( const uint8_t *data, size_t length) override;
    size_t write( const uint8_t *data, unsigned int length, wsOpCode type);
    void write( const String &text) override;
    void write( const JsonDocument &doc) override;

private:
    OpCode httpGET();

    OpCode listen();
    OpCode wait( );
    OpCode close();
    OpCode ping();
    OpCode pong();
};

}

#endif //ARDUINO_HTTPWS_HTTPWSHANDLEWEBSOCKET_H
