//
// Created by enetheru on 17/4/20.
//
#ifndef ARDUINO
#include <cstring>
#endif

#include "httpws.h"

namespace httpws{

    OpCode Client::act(){
        if( handle ){
            return handle->resume();
        } else if( available() ){
            Serial.println("New Connection");
            return httpListen();
        }
        return OpCode::NONE;

    }

    OpCode Client::react(){
        if( handle && reaction )this->reaction( this );
        return OpCode::AVAILABLE;
    }

    OpCode Client::httpListen(){
        const auto ip = remoteIP();
        String ipstr = String( ip[ 0 ] ) + "." + ip[ 1 ] + "." + ip[ 2 ] + "." + ip[ 3 ];

        Serial.print( F("Socket: ") );
        Serial.println( String( getSocketNumber(), DEC ) );
        Serial.print(F("From: "));
        Serial.print( ipstr );
        Serial.print(F(":"));
        Serial.println( String( remotePort(), DEC ) );

        /* https://tools.ietf.org/html/rfc7231#section-4
        This specification defines a number of standardized methods that are
        commonly used in HTTP, as outlined by the following table.  By
        convention, standardized methods are defined in all-uppercase
        US-ASCII letters.

        Fielding & Reschke           Standards Track                   [Page 21]
        RFC 7231             HTTP/1.1 Semantics and Content            June 2014

           +---------+-------------------------------------------------+-------+
           | Method  | Description                                     | Sec.  |
           +---------+-------------------------------------------------+-------+
           | GET     | Transfer a current representation of the target | 4.3.1 |
           |         | resource.                                       |       |
           | HEAD    | Same as GET, but only transfer the status line  | 4.3.2 |
           |         | and header section.                             |       |
           | POST    | Perform resource-specific processing on the     | 4.3.3 |
           |         | request payload.                                |       |
           | PUT     | Replace all current representations of the      | 4.3.4 |
           |         | target resource with the request payload.       |       |
           | DELETE  | Remove all current representations of the       | 4.3.5 |
           |         | target resource.                                |       |
           | CONNECT | Establish a tunnel to the server identified by  | 4.3.6 |
           |         | the target resource.                            |       |
           | OPTIONS | Describe the communication options for the      | 4.3.7 |
           |         | target resource.                                |       |
           | TRACE   | Perform a message loop-back test along the path | 4.3.8 |
           |         | to the target resource.                         |       |
           +---------+-------------------------------------------------+-------+

        All general-purpose servers MUST support the methods GET and HEAD.
        All other methods are OPTIONAL.

        Additional methods, outside the scope of this specification, have
        been standardized for use in HTTP.  All such methods ought to be
        registered within the "Hypertext Transfer Protocol (HTTP) Method
        Registry" maintained by IANA, as defined in Section 8.1.
        https://www.iana.org/assignments/http-methods/http-methods.xhtml

        The set of methods allowed by a target resource can be listed in an
        Allow header field (Section 7.4.1).  However, the set of allowed
        methods can change dynamically.  When a request method is received
        that is unrecognized or not implemented by an origin server, the
        origin server SHOULD respond with the 501 (Not Implemented) status
        code.  When a request method is received that is known by an origin
        server but not allowed for the target resource, the origin server
        SHOULD respond with the 405 (Method Not Allowed) status code.*/
        {
            char request_method[8]{'\0'};
            readBytesUntil( ' ', request_method, 8 );
            Serial.print( request_method );

            if( strcmp( request_method, "GET" ) == 0 )method = Method::GET;
            else if( strcmp( request_method, "HEAD" ) == 0 )method = Method::HEAD;
            else if( strcmp( request_method, "PUT" ) == 0 )method = Method::PUT;
            else if( strcmp( request_method, "DELETE" ) == 0 )method = Method::DELETE;
            else return httpResponse( 501, F( "Not Implemented" ) );
        }

        { // Read the resource
            // FIXME How large do we want this to be?
            // FIXME Does it conform to spec?
            char request_resource[256]{'\0'};
            size_t length = readBytesUntil( ' ', request_resource, 256 );
            Serial.print(' ');
            Serial.print(request_resource);
            if( length >= 256 ){
                return httpResponse( 414, F( "URI Too Long" ) );
            };
            // copy to string
            resource = request_resource;
        }

        { // Read the HTTP Version
            char request_version[9]{'\0'};
            readBytes( request_version, 8 );
            Serial.print(' ');
            Serial.println( request_version );
            if( strcmp( request_version, "HTTP/1.1") != 0 ){
                return httpResponse( 505, F( "HTTP Version Not Supported" ) );
            }
        }
        readStringUntil('\n');
        return OpCode::NEW;
    }

    void Client::setHandler( Handle *h ){
        delete this->handle;
        this->handle = h;
    }

    void Client::setCallback( void (*cb)( Client * ) ){
        this->reaction = cb;
    }

    void Client::close(){
        Serial.print( F("Closing Client: "));
        Serial.println( String( getSocketNumber(), DEC ) );
        flush();
        // quickly read a bunch of bytes to try to limit how much is sitting around in buffers
        int counter = 0;
        while( available() && counter < 256 ){
            counter++;
            read();
        }
        stop();

        delete this->handle;
        this->handle = nullptr;
        this->reaction = nullptr;
    }

    bool
    Client::nextHeader( String &header, String &value ){
        if( !available() )return false;

        header = readStringUntil( '\n' );

        //break out of the loop if this is a blank line.
        if( header.startsWith( "\r" ) || header.startsWith( "\n" ) ){
            return false;
        }

        //Splitting up the header text
        int p = header.indexOf( ':' );
        value = header.substring( p + 1 );
        value.trim();
        header.remove( p, header.length() );

        // According to [RFC2616], all header field names in both HTTP
        // requests and HTTP responses are case-insensitive.
        header.toLowerCase();
        return true;
    }

    OpCode Client::httpResponse( int code, Map< String, String > headers, const String &message ){

        String response = String( F( "HTTP/1.1 " ) ) + String( code, DEC ) + F( " " ) + message;
        println( response );

        printHeader( {F( "Server" ), F( "Hyperion" )} );
        printHeader( {F( "Content-Location" ), resource} );
        for( const auto &header : headers ) printHeader( header );

        print( F( "\r\n" ) );
        if( code < 400 ) return OpCode::CONTINUE;
        return OpCode::ERROR;
    }

    void Client::printHeader( const Pair< String, String > &header ){
        print( header.first );
        print( F( ": " ) );
        println( header.second );
    }

    OpCode Client::httpResponse( int code, const String &message ){
        return httpResponse( code, {0, nullptr}, message );
    }

}