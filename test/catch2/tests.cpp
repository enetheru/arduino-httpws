//
// Created by enetheru on 27/11/20.
//

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <cstring>
#include <catch2/catch.hpp>
#include <httpws_ws_handshake.h>

/* So I've created this dodgy test case just to get started with catch2,
 * but there is a problem with it such that the addition of a null
 * terminator on the string isnt catered for in WebSocketHandshake::generate
 * So whilst this test case runs OK in its current form, I dont like it.
 */
TEST_CASE( "Websocket Handshake", "[websocket]" ) {
    char key[25]{"dGhlIHNhbXBsZSBub25jZQ==" };
    char hash[29]{};
    WebSocketHandshake::generate( key, hash );

    REQUIRE( std::strcmp( hash, "s3pPLMBiTxaQ9kYGzzhZRbK+xOo=" ) == 0 );
}

/* I need to build tests now.
 * httpws::Handle
 * httpws::Client
 * httpws::Server
 * httpws::HandleSdFat
 * httpws::HandleWebSocket
 * templates
 *  Array
 *  Pair
 *  Map
 *  CircleArray
 *  stubs?
 */