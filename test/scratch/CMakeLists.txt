add_executable(scratch main.cpp)

if( ARDUINO )
    target_link_libraries(scratch PRIVATE arduino-httpws)
    target_link_arduino_libraries( scratch PRIVATE core)
    target_enable_arduino_upload( scratch )
else()
    target_link_libraries(scratch PRIVATE arduino-httpws stubs)
endif()
