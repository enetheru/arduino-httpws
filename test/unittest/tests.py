""" Test Harness for the arduino-httpws server"""

import unittest
import requests
import socket
import aiohttp
import asyncio

# server_address = '10.0.0.177'
server_address = "httpws_test.local"
server_name = ""


def print_request(r):
    print("Request :", r.request.method, r.request.url)
    print("Headers :", r.request.headers)
    print("Response:", r.status_code, r.reason)
    print("Headers :", r.headers)


class HttpRequest(unittest.TestCase):
    """Test the acceptance of data on the port and handoff to resource handlers."""
    # TODO Test the various HTTP Methods failing on not implemented ones

    def test_no_resource_handler(self):
        """When a resource handler doesnt exist for a path does it fail gracefully"""
        r = requests.get(f'http://{server_address}/missing-file')
        print_request(r)
        self.assertEqual(404, r.status_code)

    @unittest.skip("partially implemented")
    def test_request(self):
        """When the initial request isnt formed correctly does it reject the connection?
        For this I need to make a raw socket connection and send it bullshit data
        TODO fail on invalid request method
        TODO fail on first line too large
        TODO fail on invalid http version"""
        sock = socket.create_connection((server_address, 80), timeout=3)
        sock.send(b'Bullshit Data')
        while True:
            data = sock.recv(1024)
            if data == b'':
                print("connection closed")
                break
            else:
                print(data)
                self.assertTrue(data.startswith(b'HTTP/1.1 400'))
                break
        sock.close()


class HttpSdFatGet(unittest.TestCase):
    """Test GET requests using the SdFat resource handler"""

    def test_file(self):
        """Get a small file"""
        r = requests.get(f'http://{server_address}/small_file')
        print_request(r)
        print("Body    :", r.text)
        self.assertEqual(200, r.status_code)
        # TODO Compare Contents

    def test_file_in_subtree(self):
        """GET a file that resides in a subtree"""
        r = requests.get(f'http://{server_address}/trunk/branch/leaf')
        print_request(r)
        print("Body    :", r.text)
        self.assertEqual(200, r.status_code)
        # TODO Compare Contents

    def test_file_large(self):
        """GET a large file"""
        r = requests.get(f'http://{server_address}/large_file')
        print_request(r)
        self.assertEqual(200, r.status_code)
        # TODO Compare Contents

    def test_file_concurrent(self):
        """Concurrently GET multiple large files"""
        # Without the help of the article I would not have been able to make sense of the aiohttp library
        #   https://pawelmhm.github.io/asyncio/python/aiohttp/2016/04/22/asyncio-aiohttp.html
        async def fetch(url, session):
            async with session.get(url) as response:
                await response.text()
                return response

        async def run():
            url = f'http://{server_address}/large_file'

            # Fetch all responses within one Client session,
            # keep connection alive for all requests.
            async with aiohttp.ClientSession() as session:
                tasks = []
                for i in range(4):
                    tasks.append(asyncio.ensure_future(fetch(url, session)))

                responses = await asyncio.gather(*tasks)
                for response in responses:
                    self.assertEqual(response.status, 200)
                    self.assertEqual(await response.text(), await responses[0].text())

        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.ensure_future(run()))

    def test_directory_root(self):
        """GET directory tree and return 200 OK"""
        r = requests.get(f'http://{server_address}/')
        print_request(r)
        print("Body    :", r.text)
        self.assertEqual(200, r.status_code)
        # TODO Parse and compare directory tree

    def test_file_not_found(self):
        """GET invalid resource record, return a 404"""
        r = requests.get(f'http://{server_address}/file_not_found')
        self.assertEqual(404, r.status_code)

    @unittest.skip("not yet implemented")
    def test_hidden_file(self):
        """GET a hidden file and return 200"""
        r = requests.get(f'http://{server_address}/.hidden')
        self.assertEqual(200, r.status_code)

    @unittest.skip("not yet implemented")
    def test_tree_hidden_file(self):
        """GET a directory index omitting a hidden file"""
        # TODO test returned body for hidden file location

    @unittest.skip("not yet implemented")
    def test_no_permission(self):
        """GET a record with no read permission and return ???"""
        r = requests.get(f'http://{server_address}/not_permitted')
        self.assertEqual(403, r.status_code)

    @unittest.skip("not yet implemented")
    def test_early_client_termination(self):
        """TODO What happens when the client cancels the connection early"""
        self.assertEqual(True, False)

    def test_endless_headers(self):
        """Be robust against endless headers"""
        sock = socket.create_connection((server_address, 80), timeout=30)
        sock.send(b'GET / HTTP/1.1\n')
        with self.assertRaises(BrokenPipeError):
            while sock:
                sock.send(b'False-Header:False-Data\n')
        sock.close()


class HttpSdFatHead(unittest.TestCase):
    """ Test HEAD requests using the SdFat handler"""
    # TODO perform all the same tests as GET but return only the header and no body.

    @unittest.skip("not yet implemented")
    def test_headers(self):
        """TODO all the tests"""


class HttpSdFatPut(unittest.TestCase):
    """ Test PUT requests using the SdFat handler
        TODO: curl -vX PUT 10.0.0.177/name-of_resource @/path_to_file
          Depending on the filesize(I'm not sure how large) curl will request a
          100-continue response before sending the data.
          NOTE: need to specify --data-binary rather than -d/-data
          otherwise line endings are stripped by curl when sending. I havent tested
          but i think -T does not suffer from this problem"""

    def test_put_file_small(self):
        """PUT a small file on sdcard"""
        url = f'http://{server_address}/put_small_file'
        content = "put small file contents"
        r = requests.put(url=url, data=content)
        print_request(r)
        self.assertIn(r.status_code, [201, 204])
        s = requests.get(url)
        self.assertEqual(200, s.status_code)
        print_request(r)
        print("Text    :", s.text)
        self.assertEqual(content, s.text)

    def test_put_file_large(self):
        """PUT a large file on sdcard"""
        url = f'http://{server_address}/put_large_file'
        content = open('./SD_Content/large_file', 'rb')
        r = requests.put(url=url, data=content)
        content.close()
        print_request(r)
        self.assertIn(r.status_code, [201, 204])
        s = requests.get(url)
        self.assertEqual(200, s.status_code)
        print_request(r)
        content = open('./SD_Content/large_file', 'r')
        self.assertEqual(content.read(), s.text)
        content.close()

    def test_put_file_subtree(self):
        """PUT a small file in a subtree on sdcard"""
        url = f'http://{server_address}/missing_subtree/leaf'
        content = open('./SD_Content/small_file', 'rb')
        r = requests.put(url=url, data=content)
        content.close()
        print_request(r)
        if r.headers['Server'].startswith('nginx'):
            self.skipTest("nginx does not support creating subtrees")
        self.assertIn(r.status_code, [201, 204])
        s = requests.get(url)
        self.assertEqual(200, s.status_code)
        print_request(r)
        content = open('./SD_Content/small_file', 'r')
        self.assertEqual(content.read(), s.text)
        content.close()

    @unittest.skip("not yet implemented")
    def test_put_file_multi(self):
        """Concurrently PUT large files on sdcard"""
        # Without the help of the article I would not have been able to make sense of the aiohttp library
        #   https://pawelmhm.github.io/asyncio/python/aiohttp/2016/04/22/asyncio-aiohttp.html
        async def request(url, session):
            # TODO put file contents
            async with session.put(url) as response:
                return response

        async def run():
            # PUT all files within one Client session,
            # keep connection alive for all requests.
            async with aiohttp.ClientSession() as session:
                tasks = []
                for i in range(4):
                    tasks.append(asyncio.ensure_future(request(f'http://{server_address}/put_multi_{i}', session)))

                responses = await asyncio.gather(*tasks)
                for response in responses:
                    self.assertIn(response.status, [201, 204])

        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.ensure_future(run()))
        # TODO test PUT'd content for correctness

    @unittest.skip("not yet implemented")
    def test_put_file_subtree_fail(self):
        """PUT a small file into a missing subtree"""
        # TODO This test requires changes to the server to allow additional
        #  headers to specify that we dont want to automatically create the subtree

    @unittest.skip("not yet implemented")
    def test_put_file_overwrite(self):
        """PUT a file on sdcard overwriting existing file"""
        # TODO Create test

    @unittest.skip("not yet implemented")
    def test_put_file_readonly(self):
        """PUT a file on sdcard and fail to overwrite due to readonly permissions"""
        # TODO Create test

    @unittest.skip("not yet implemented")
    def test_put_subtree(self):
        """TODO PUT a subtree on sdcard"""


class HttpSdFatDelete(unittest.TestCase):
    """ Test DELETE requests using the SdFat handler"""
    # TODO fail to delete readonly
    #
    def setUp(self) -> None:
        r = requests.put(f'http://{server_address}/del_small_file')
        print_request(r)
        self.assertIn(r.status_code, [201, 204])
        # nginx cannot use PUT to create a directory
        # WebDAV has an additional verb called MKCOL which is Make Collection
        # which is quivalent to mkdir but requsts does not support WebDAV verbs out of the box.
        print("setup complete")

    def test_delete_file(self):
        """ TODO delete file on sdcard
            missing file
            readonly
            success"""
        url = f'http://{server_address}/del_small_file'
        r = requests.delete(url=url)
        print_request(r)
        self.assertEqual(r.status_code, 204)
        s = requests.get(url)
        self.assertEqual(404, s.status_code)
        print_request(r)

    def test_delete_dir(self):
        """ TODO delete whole directory on sdcard
            missing dir
            readonly
            has content
            recursive
            success"""
        url = f'http://{server_address}/trunk'
        r = requests.delete(url=url)
        print_request(r)
        if r.headers['Server'].startswith('nginx'):
            self.skipTest("nginx does not support deleting subtrees")
        self.assertIn(r.status_code, [201, 204])
        s = requests.get(url)
        self.assertEqual(404, s.status_code)
        print_request(r)

    @unittest.skip("Not yet implemented")
    def test_delete_readonly_fail(self):
        """DELETE readonly file and fail"""
        # TODO Implement test


class HttpGetWebSocket(unittest.TestCase):
    """Websocket Testing"""
    # TODO Upgrade
    # TODO close connection
    # TODO Ping/pong
    # TODO message smallest
    # TODO message medium
    # TODO message large
    # TODO message text
    # TODO message binary
    # TODO message multi-part
    @unittest.skip("not yet implemented")
    def test_get_upgrade(self):
        """TODO websocket upgrade request"""
        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
