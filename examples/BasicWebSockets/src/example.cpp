// Write your code here
#include <Arduino.h>

#include <httpws.h>
#include <httpws_handle_sdfat.h>
#include <httpws_handle_websocket.h>

//FIXME move to somewhere relevant
#ifdef ARDUINO
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

static int freeMemory(){
    char top;
#ifdef __arm__
    return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
    return &top - __brkval;
#else  // __arm__
    return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__

}
#endif

httpws::Server server{80};

void websocketReceive( httpws::Client *client ) {
    Serial.println(F("Received websocket message."));
    Serial.print(F("memory: "));
    Serial.println( freeMemory() );
    uint8_t value;
    while( client->getHandle()->read( value ) ){
        Serial.print( char(value) );
    }
    Serial.println();

    client->getHandle()->write( F("Return Message.") );
}

void setup(){
    EthernetClass::begin( nullptr,
            IPAddress(10,0,0,177),
            IPAddress(0,0,0,0),
            IPAddress(0,0,0,0),
            IPAddress(255,255,255,0) );

    yield();
    delay(1000);


    Serial.begin( 9600 );
    yield();
    delay(1000);

    Serial.println(F("\nSETUP FUNCTION"));
    static httpws::Client clients[4]{}; //data backing client list

    static Pair<String, httpws::Server::Binding> bindings[]{
            {F("/websocket"), {&httpws::HandleWebsocket::factory, &websocketReceive }},
            {F("/"),{&httpws::HandleSdFat::factory, nullptr } }
    };

    server.assignClients( {4, clients } );
    server.assignBindings( {2, bindings} );
    server.begin();
    yield();
    delay(1000);

    Serial.println(F("\nSETUP COMPLETE"));
}

void loop(){
    static auto time = millis();
    unsigned long old_time = time;
    time = millis();
    static int oldMem = 0;
    int freeMem = freeMemory();
    if( oldMem != freeMem ){
        Serial.print("Free Memory: ");
        Serial.println( freeMem );
        oldMem = freeMem;
    }
    //Serial.println("\n== Main Loop ==");

    httpws::OpCode code = server.read();
    if( code == httpws::OpCode::NONE) return;

    Serial.println( F("---" ) );
    Serial.print( F("Server.read() result: ") );
    switch( code ){
        case httpws::OpCode::NEW:
            Serial.println(F("Received new HTTP Request on client"));
            break;
        case httpws::OpCode::CONTINUE:
            Serial.println(F("Continuing HTTP Request on client"));
            break;
        case httpws::OpCode::CLOSE:
            Serial.println(F("Completed HTTP Request on client"));
            break;
        case httpws::OpCode::AVAILABLE:
            Serial.println(F("Has available data to read on client"));
            break;
        case httpws::OpCode::ERROR:
            Serial.println(F("Error"));
            break;
    }
//------------
    Serial.print(F("Loop Duration: "));
    Serial.println( String(time - old_time));
    Serial.println( F("---") );
}
