//
// Created by enetheru on 13/11/20.
//

#ifndef ARDUINO_HTTPWS_HANDLE_DUMMY_HPP
#define ARDUINO_HTTPWS_HANDLE_DUMMY_HPP

#include "httpws.h"

class HandleDummy : public httpws::Handle {

public:
    explicit HandleDummy(httpws::Client *client);
    static Handle *factory(httpws::Client *client);
    ~HandleDummy() override = default;

    httpws::OpCode resume() override;

    // Print override
    size_t read( uint8_t & ) override;

    // Text write
    size_t write( uint8_t value ) override;
    size_t write( const uint8_t *data, size_t length ) override ;
    void write( const String &text ) override;
    void write( const JsonDocument &doc ) override;

};

#endif //ARDUINO_HTTPWS_HANDLE_DUMMY_HPP
