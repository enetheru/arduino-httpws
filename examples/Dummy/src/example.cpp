//
// Created by enetheru on 9/11/20.
//

#include <Arduino.h>
#include <httpws.h>
#include "handle_dummy.hpp"

void dummyCallback( httpws::Client *client ) {
    // This does nothing
}

httpws::Server server{80};

void setup(){
    Serial.begin( 9600 );
    yield();
    delay(1000);

    Serial.println(F("Dummy Setup"));

    EthernetClass::begin( nullptr,
                          IPAddress(10,0,0,177),
                          IPAddress(0,0,0,0),
                          IPAddress(0,0,0,0),
                          IPAddress(255,255,255,0) );

    yield();
    delay(1000);

    //data backing client list
    static httpws::Client clients[4]{};

    static Pair<String, httpws::Server::Binding> bindings[]{
            {F("/"), {&HandleDummy::factory, &dummyCallback }}
    };

    server.assignClients( {4, clients } );
    server.assignBindings( {1, bindings} );
    server.begin();
    yield();
    delay(1000);

    Serial.println(F("Setup Completed"));

}

void loop(){
    static auto time = millis();
    unsigned long old_time = time;
    time = millis();
    //Serial.println("\n== Main Loop ==");

    httpws::OpCode code = server.read();
    if( code == httpws::OpCode::NONE) return;

    Serial.print( F("Server read result: : ") );
    switch( code ){
        case httpws::OpCode::NEW:
            Serial.println(F("Received new HTTP Request on client"));
            break;
        case httpws::OpCode::CONTINUE:
            Serial.println(F("Continuing HTTP Request on client"));
            break;
        case httpws::OpCode::CLOSE:
            Serial.println(F("Completed HTTP Request on client"));
            break;
        case httpws::OpCode::AVAILABLE:
            Serial.println(F("Has available data to read on client"));
            break;
        case httpws::OpCode::ERROR:
            Serial.println(F("Client error connection closed"));
            break;
    }
//------------
    Serial.print(F("Loop Duration: "));
    Serial.println( String(time - old_time));
    Serial.println( F("\n---") );

}