//
// Created by enetheru on 13/11/20.
//

#include "handle_dummy.hpp"

HandleDummy::HandleDummy( httpws::Client *client ){

}

httpws::Handle *HandleDummy::factory( httpws::Client *client ){
    return new HandleDummy(client);
}

httpws::OpCode HandleDummy::resume(){
    return httpws::OpCode::ERROR;
}

size_t HandleDummy::read( uint8_t & ){
    return 0;
}

size_t HandleDummy::write( uint8_t value ){
    return 0;
}

size_t HandleDummy::write( const uint8_t *data, size_t length ){
    return 0;
}

void HandleDummy::write( const String &text ){
    // do nothing
}

void HandleDummy::write( const JsonDocument &doc ){
    //do nothing
}


