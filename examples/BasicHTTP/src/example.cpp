//
// Created by enetheru on 9/11/20.
//

#include <Arduino.h>
#include <Ethernet.h>
#include <httpws.h>


const IPAddress
    ip{10,0,0,177},
    dns{0,0,0,0},
    gateway{0,0,0,0},
    subnet{255,255,255,0};

httpws::Server server{80};

void setup(){
    EthernetClass::begin( nullptr, ip, dns, gateway, subnet );

    yield();
    delay( 1000 );

}

void loop(){

}