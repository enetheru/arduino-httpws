Fri 17 Apr 2020 21:13:29
-----------------------------
OK so a little bit of a paradigm shift in the design to separate the resource
handle from the client such that it becomes generic.

it looks like resource handle will have some default functions to deal with.
the thing i'm trying to figure out is whether it generates a new resource
handle and uses that, or whether it has a base class.

I'm trying to keep everything on the stack as much as possible. which means
pre-declaring things and then referencing them with pointers.

how can i main state within the client, for the position within the resource
handle that it has..

Currently i do this with very little, a last opcode, and a function pointer to
know what happens next. perhaps i can maintain the same paradigm.

Enetheru: Wed 04 Nov 2020 12:41:09
----------------------------------
re-visiting this project with the arduino-cmake-toolchain project
implementation worked a treat, had to really change up a bunch of things and
cherry pick all the work I performed on the paltformio branch i had.. that is
all removed now.


From here on, I plan on keeping the Arduino IDE and the Arduino-Cmake-Toolchain
methods working.

If I can find a way to get platformIO working aswell I will try, but so far
it's too different.

Last I knew, I had finished hacking in resource handles to work, but I was
burned hard by the way I did it so took a break, I will review past changes and
see whats up.

Enetheru: Mon 09 Nov 2020 13:16:47
----------------------------------
I've spent a bunch of time fiddling around with catch2 testing framework, and
while I can get multi arch building happening in clion, the volume of work
needed to create abstraction away from the arduino platform is too much for me
right now.

I shifted focus to conforming to the arduino library specification, adding
examples which utilise the structure.

I want to be working on simplifying the code so that only a single header is
needed to include, creating a test suite and generally making it easier to use.

I also want to make some way to disable logging such that it contributes
nothing to the code. The only way I can think so far is to use a macro, which I
would prefer to avoid.

Enetheru: Tue 10 Nov 2020 13:20:00
----------------------------------
Whilst working on this refactor of the HttpOK and HttpError functions to remove
the template construction, I'm noticing a trend that the content-location
header is almost always present, so I would be interested in seeing all
locations and perhaps factoring that into the main function rather than have to
add it each time. If it is indeed the case that its always being added.

It's so frustrating to not have the stdlib available to use. All I really need
is to be able to create arrays on the stack from an initialiser list.

I also want to create arrays and pass them to a function and have them take the
temporary, I guess I need more constructors.

Well after a bunch of experimentation it appears that the challenge to create
initialisation lists that allow a constructor to take a temporary to build up
complex objects is not worth it, or impossible. So I settled on an easier
approach, whilst also consolidating the ideas. I've tested and it still works,
so I'm committing to the repo.

---

It appears that last time I was re-factoring to get resource handlers working I
broke websocket connections. Sigh.

It appears that the pointers to the resource handles get set before there is a
resource handle to set it to. I have no idea how it could possibly work.

client->act()
    if no handler or action
        client->listen()
        set pointers to resource handler GET/POST/etc
        return TYPE
server->check return type
    client->bind resource handler for client.

That's backwards.

Enetheru: Wed 11 Nov 2020 15:49:53
----------------------------------
Further investigation, it appears that whats actually happening is that it
pointing to the base class virtual table, which of course is being overridden
by the subclass. Hence why the code works at all.

Still I'd like to refactor the order in which things happen as that's still
confusing to observe.

Enetheru: Thu 12 Nov 2020 17:56:01
----------------------------------
```
Without this available() call, the server stops working after the first client is closed.
When a client calls stop(), it makes a call to Ethernet.socketClose() function,
so a subsequent Ethernet.socketBegin() call needs to be made, but they are private.
Ethernet.socketClose(index) calls server.server_port[index] = 0 //GROSS

available() loops through EthernetServer.server_port[]
checks for whether the port is in use by (server_port[index] == _port)
if it is in use, checks the status
     if it has data available to read, sets the sockindex to index
     if it is closed remotely, disconnects socket
     if a port is listening sets listening to true
     if its closed sets server_port[] to 0;
if no ports are listening then calls begin();
returns a new EthernetClient(socketIndex)

server.begin() calls Ethernet.socketBegin( TPC, ListenPort ) which returns socketIndex
it then runs socketListen(socketIndex) to set the socket to a listening state
If its listening it assigns the port number to server_ports[socktIndex]
if it not listening then it calls Ethenet.socketDisconnect(socketIndex)

Ethernet.socketBegin() first looks for a closed socket and then goto: makesocket
failing that it find a closing socket and goto: closemakesocket
failing that it returns MAX_SOCK_NUM
```

Enetheru: Thu 12 Nov 2020 19:44:13
----------------------------------
I think that since I have SD Card functionality its highly possible to move all
the error strings to a file on the SD card and use that instead. it would
reduce PROGMEM dramatically. and reduce RAM usage too as it never needs to load
a full string, just stream the bytes.

A job for another time.

Something like a constexpr map that resolves the string at compile time using
the strings themselves as the key.. not sure if that can be possible., I asked
[reddit](https://www.reddit.com/r/arduino/comments/jsrqx3/constexpr_map_with_progmem_or_sdfat_backed_memory/)

Enetheru: Fri 13 Nov 2020 10:49:57
----------------------------------
Now that I have websocket message passing working again, its time to continue
to flesh out things and audit things, and write tests.

Whilst performing previous refactors I noticed that there was some strangeness
in how the handlers actions are setup.

the client would detect what method was being used, and then set the initial
action inside the client itself to some http method which was then pointing
into the handler

for the websocket connection this setup was then duplicated to handle the
additional websocket things.

i have stripped the client of all specific methods and shifted that
responsibility entirely to the handler.

Now, the client looks at the first line only to get the resource and the method
and stores them both as a string and an enum respectively

the server then checks the resource for an appropriate handler
the handler factory then checks the method to tell itself what action to take.
so much simpler control flow.

I'm almost of a mind to create separate handlers discriminating both path and
method, to reduce memory footprint of a connection.

---

OK so far so good, now I want to make a test suite to check against, somthing I
can load up in a web browser and run and it will just go through checks.

### basic operation
* networking
    - simultaneous connections
    - too many connections
    - ping
* server Information
    - using the OPTIONS method
* basic request failures
    - malformed http requests
    - early termination
    - no headers
    - endless headers
    - broken headers

### SdFat Handler
* GET
    - file
    - large file
    - folder tree
    - fail on missing file
    - fail on missing tree
    - fail on hidden/permissions
* HEAD
     -same as GET, but without data transfer
* PUT
    - create file
    - create large file
    - create subtree
    - overwrite file
    - fail on readonly
    - fail on missing parent folder
* DELETE
    - delete file
    - delete folder
    - delete tree
    - fail on missing file
    - fail on missing folder
    - fail on readonly
* POST
    - ?
* Concurrent variations of all the above

### Websocket Handler
* handshake
* mutltiple connections
* ping
* pong
* disconnect
* stream messages from sdcard
* stream messages to sdcard

### custom handler
* ???

Enetheru: Sun 15 Nov 2020 13:09:33
----------------------------------
Having a difficult time motivating myself to write these test cases. I think it
might be due to a lack of specificity on my part to truly define what it is
that I wish to build.

So far I have some javascript that builds a basic website with labels and
buttons.

The buttons run the function, but I need them to update a span.

If I click the button, it should run the associated test function and depending
on the result update the associated span with the result.

That means that it needs to know its own parent, so it can pick the span.

---

Ugh, so annoying, switching gears from c++ to javascript is such a jerky
contrast.

Do I create custom events handlers and then dispatch events to each element.
That still requires me to attach the test function somehow to the element.
But at least then I can just query select the DOM elements and dispatch events
to them.

Actually that's probably a bad idea, because I need to run the tests
sequentially.

It's probably better that I assign a unique ID to each test as I create them,
and then run the tests in a standard loop.

Enetheru: Wed 18 Nov 2020 09:27:51
----------------------------------
fuck me I really don't want to write javascript. I have to bite the
bullet and create this test page if I ever want to progress.

I have really janky functions to create list items and the ability to iterate
over them whilst running tests. I have to get the test result and use that to
change the outcome. 

Enetheru: Thu 19 Nov 2020 20:28:58
----------------------------------
testsare already working, even the first attempt at getting this testing page
working has led to forcing me to discover a really hard to track bug caused by
shadowing a local variable with a function argument of the same name.

Enetheru: Fri 20 Nov 2020 18:47:21
----------------------------------
So after working on this testing page, I soft of want everything to be run with
a sort of priority task list that waits for the previous task to complete
before continuing, my ghetto setup right now blocks the whole browser till it
times out. I'd rather that not be manually built but rather of the task
scheduler.

It's just that my time is probably better spent making the tests themselves.

Enetheru: Sat 21 Nov 2020 13:28:41
----------------------------------
It's slowly becoming clear that perhaps making the test suite from javascript
within a browser context doesn't give me enough capabilities to inspect and
gather information that I want to.

I'm wondering to self whether to switch over to ctest and code everything up in
there, or write something custom for myself. It's just too fucking hot to think
today, and lockdown wont lift till midnight. Perhaps I'm asking too much of
myself.

Enetheru: Wed 25 Nov 2020 14:31:08
----------------------------------
After much experimentation I ended up creating stubs for all functions used in
my code so that it might compile and link on x86. So far so good.
I have cleaned up some of the cmake code as well. This will allow me to use
standard libraries for testing, and test frameworks. So now I have to evaluate
the available c++ test frameworks and pick one to implement.

As far as testing the server responses go, I still believe javascript it
perhaps the wrong way to go, and that I would have better luck with python.
Will see. Time to commit my progress.

---
Continuing on, clion currently supports the following frameworks
* Boost.Test
* Catch/Catch2
* Doctest
* Google Test

And maybe ctest? In the next version 2020.3.EAP, and I have 2020.2, not sure
what EAP is about... Searches... Aha! Early access program.

OK so I can opt into the early access program with a change of source repo,
perhaps, will see.

So I can rule out google and boost, google for the  monopoly, and boost because
I never use boost due to its bloated corpse scumming up dependency trees.

That leaves ctest, doctest, and catch2.

Looks like doctest is derivative work of catch2, even stating that it lifts
some code directly. But something that makes me wonder is that it boasts
putting test code directly into the production source, and I'm not sure that I
can support that due to the architecture of the system.

It's a toss up, and I like the look of Doctest's documentation, but I have
doubts about fitness for purpose. Where as I don't like the look of catch2's
documentation, but I doubt its fitness less. I guess for once I will not go for
the shiny and choose catch2 to begin my testing.

---
what the fuck, no reason, it just stopped compiling and is unable to find my
library any longer, like I didn't change anything.. so annoying. I think its
time to reboot my machine.

Enetheru: Fri 27 Nov 2020 12:18:57
----------------------------------
Fixed the build, it was some cmake mangling needed to be done to support both
x86 and arduino.

I've now started on integration of catch2, and its easy to get the beginnings,
but it makes me think about my stubs, and whether to include all the libs into
the source tree for testing, I've tested ArduinoJson already and that works.
SdFat doesn't have any cmake support, and I suspect that it will just have too
many missing things as it integrates more tightly with the hardware.

As I think about the stubs, I'm unsure whether I will create a defacto
re-implementation which allows me to test on x86, I mean how much functionality
will I have to build into it by default so that I can test things?

Enetheru: Sat 28 Nov 2020 10:44:47
----------------------------------
Looking at writing tests for things, and I noticed that my Array class is more
like a non owning array view, I was thinking about non owning string classes
earlier due to string literals never needing to be allocated into ram.

I think I want to make some tests on that.

Enetheru: Sun 29 Nov 2020 14:15:24
----------------------------------
Things are slowly moving, I have decided to try to remove the Arduino String
library from my code unless its completely necessary. I don't like the unknown
memory consumption that it allows, and after looking at the code I don't like the
way its built.

I would rather build up and tear down stack buffers, even if its an initial
over allocation. So long as I have control over the growth of the memory. I
even wonder if in some cases it might be useful to use the heap to
gather the data, then copy it to the stack so that RAII works, I mean the only
consideration would be heap fragmentation.

I wonder if there is a way to tear down a stack buffer, but keep its memory
around so that when you rebuild it you allocate a larger array size.
Or rather dynamically increase the size of a stack array because it exists in a
new scope.

Or am I procrastinating writing these tests.

Enetheru: Tue 08 Dec 2020 15:40:03
----------------------------------
Reviewing my tests so far, and I'm surprised to finally learn exactly what
WebDAV is and how it relates to the whole ecosystem, and also surprised that
its so shit.

So considering its so bad and I am basically just uploading stuff, I'm going to
break from the standards and allow recursive DELETE and PUT that creates parent
directories.

But that means I cannot test against against Nginx or Apache any of the
functionality that diverges from standard HTTP and WebDAV verbs. But I don't
plan on diverging that much.

I'll have to change the way I think about writing tests now.

I want to support the things that are normally supposed to work, so there are
no surprises, I'll just support additional things too. Which means that I will
need to create conditional tests based on the platform I'm testing against.
This will be with somthing like a get reqest and a test against the server name
or type.

Either way I need a loo break and a walk around.

Enetheru: Fri 11 Dec 2020 11:08:12
----------------------------------
I need to figure out the protocol usages that I wish to support so that I can
move forward with my testing regime, and how to perform conditional tests with
python.unittest so that I can skip those tests on nginx/apache/etc.

I want my PUT method to be able to create parent folders, empty folders, and I
want my DELETE method to be able to delete whole subtrees

Perhaps the way I can support these things with with using headers to indicate
that they are allowed. Support default behaviour out of the box, and then with
a header support the above.

There doesn't appear to be anything in the standard that prevents me from
implementing it this way. I don't even need the headers. But conditional tests
are required to make the test regime work for different servers.

I can use the 'Server' header to identify my target.

Nginx WebDAV appears to just be broken or unsupported so I cannot rely on it
for a lot of the testing.
