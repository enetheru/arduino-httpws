#!/bin/bash

mkdir -p build
cd build
cmake -D CMAKE_TOOLCHAIN_FILE=../cmake/Arduino-CMake-Toolchain/Arduino-toolchain.cmake ../
