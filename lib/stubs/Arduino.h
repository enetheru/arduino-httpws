//
// Created by enetheru on 22/11/20.
//

#ifndef ARDUINO_ARDUINO_H
#define ARDUINO_ARDUINO_H
#include <string>
#include <cstddef>


#define F(i) i
#define DEC 10
#define HEX 16
#define BIN 2

using byte = uint8_t;

void yield();
void delay( int );
int millis();
int min( int, int );
int freeMemory();
void setup();
void loop();

class IPAddress{
    union{
        uint8_t bytes[4];
        uint32_t dword;
    }address;

public:
    IPAddress( uint8_t, uint8_t, uint8_t, uint8_t );
};

class String{
    char * buffer_{nullptr};
    unsigned int capacity_{0};
    unsigned int length_{0};
public:
    typedef char * iterator;
    typedef const char *const_iterator;

public: //Rule of Five
    String() = default;
    String( const char * source );
    ~String();
    String( const String & );
    String( String && ) noexcept;
    String& operator=( const String & );
    String& operator=( String&&) noexcept;
    //------------------
    String( const char *, int );
    String( int, int );
    String( unsigned char );

    String  operator+( char * ) const { return "";}
    String  operator+( unsigned char ) const { return "";}
    String  operator+( const String& ) const { return "";}
    String  operator+=( const String& ){ return ""; }
    bool    operator!=( const String& ){ return false; }
    operator bool(){ return false; }

    int lastIndexOf( char ) { return 0; };
    int indexOf( char ) const { return 0; }
    bool startsWith( const String& ) const{ return false; };
    int length() const{ return 0; };
    String substring( int, int) { return ""; };
    String substring( int ) { return ""; };
    void trim(){}
    void toLowerCase(){};
    void remove( int, int ) {}
    const char * c_str() const{ return buffer_; }
    bool endsWith( String ){ return false; }
    int compareTo( String ){ return -1;}
    void setCharAt( int, char ){}
    int toInt(){return 0;}
    bool equals(String){return false;}

    iterator begin() const {return nullptr;}
    iterator end() const {return nullptr;}

};

class Print {
public:
    virtual size_t write( uint8_t ){return 0;};
    virtual size_t write( const uint8_t*, size_t ){return 0;};
    virtual size_t print( String );
    virtual size_t println( String );
    virtual size_t println(){ return 0;}
    String readStringUntil(const char){ return ""; };
};

class Serial_class : public Print{
public:
    void begin( int ){}
};

extern Serial_class Serial;



class EthernetClass{
public:
    static void begin(void *, IPAddress, IPAddress, IPAddress, IPAddress){};
};

class EthernetClient: public Print {
public:
    EthernetClient() = default;
    EthernetClient( int i ){}

    uint8_t *remoteIP(){ return nullptr; }
    int remotePort(){return 0;}
    int getSocketNumber() const{ return 0; }
    bool available(){ return false;}
    void flush(){}
    void stop() {}
    int read(){ return 0; }
    int read( uint8_t*, int){ return 0; }
    int readBytesUntil( char, char *, int ){ return 0; }
    int readBytes( char *, int ){ return 0; }
    int write( const uint8_t *, int ){return 0;}
    size_t write( const uint8_t ){return 0;}

    bool connected(){return false;}
};

class EthernetServer{
public:
    EthernetServer( int ){};
    void available() {}
    void begin(){}
};

class JsonDocument{
public:
};

unsigned long measureJson( JsonDocument );
void serializeJson( const JsonDocument &, EthernetClient );


#endif //ARDUINO_ARDUINO_H
