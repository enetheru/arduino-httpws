//
// Created by enetheru on 24/11/20.
//

#ifndef ARDUINO_SDFAT_H
#define ARDUINO_SDFAT_H

#define SD_SCK_MHZ(n) 0
#define O_RDONLY 0
#define O_WRITE 0
#define O_TRUNC 0
#define O_CREAT 0
#define O_SYNC 0
#define O_READ 0

struct File{
    void open( const char *, int){}
    bool isOpen(){return false;}
    bool isFile(){return false;}
    char * getName(const char *, int){ return nullptr; }
    int size(){return 0;}
    bool isDir(){return false;}
    void close() {}
    bool isReadOnly(){return false;}
    bool rmRfStar(){return false;}
    bool remove(){return false;}
    bool available(){ return false; }
    int read(){return 0;}
    int read( uint8_t *, int){ return 0; }
    bool openNext( File *, int ){return false;}
    int write( uint8_t *, int ){ return 0;}
    void sync(){}
    int position(){return 0;}
};

struct Volume {
    bool isOpen() {return false;}
    void sync(){}
};

struct SdFat{
    bool begin( int, int ) { return false; }
    bool cardErrorCode() { return false;}
    char *cardErrorData() { return nullptr; }
    bool fatType(){ return false; }
    Volume *vwd(){ return nullptr; }
    bool exists( const char *){ return false;}
    bool mkdir( const char* ){ return false; }
    bool chdir( const char*, bool ){ return false; }
    bool remove(const char *) { return false; }
};


#endif //ARDUINO_SDFAT_H
