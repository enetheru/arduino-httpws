//
// Created by enetheru on 27/11/20.
//

#include <cstring>
#include <utility>
#include "Arduino.h"

// Rule of Five
String::String(const char* source){
    std::size_t n = std::strlen(source) + 1;
    buffer_ = new char[n];
    std::memcpy(buffer_, source, n);
}

String::~String(){
    delete[] buffer_;
}

String::String(const String& source) // copy constructor
        : String(source.buffer_){}

String::String(String&& source) noexcept // move constructor
        : buffer_(std::exchange(source.buffer_, nullptr)) {}

String &
String::operator=(const String& source){ // copy assignment
    return *this = String(source);
}

String &
String::operator=(String&& source) noexcept { // move assignment
    std::swap(buffer_, source.buffer_);
    return *this;
}

//---

String::String( const char *, int ){

}

String::String( int, int ){

}

String::String( unsigned char ){

}