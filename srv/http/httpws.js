export {Test};

class Test {
    constructor(name, description, func){
        this.name = name;
        this.description = description;
        this.func = func;
    }

    run(){
        return this.func();
    }
}
