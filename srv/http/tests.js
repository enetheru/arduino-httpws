export function networkingPing(){
    console.warn("tests.networkingPing: I currently dont have a good way to create a ping test from javascript," +
        " this might have to be offloaded to a separate thing and then the result imported? completely unsure at the moment");
    return false;
}

export function networkConcurrant(){
    let hostname = document.querySelector("#hostname");
    let quantity = 6;
    let result = 0;
    let fail = 0;
    for( let i = 0; i < quantity; ++i ) {
        if( i >= quantity +1 )return;
        fetch(`http://${hostname.value}/`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.text();
            })
            .then(text => {
                result++;
            })
            .catch(error => {
                console.error('There has been a problem with your fetch operation:', error);
                fail++;
            });
    }

    let wait = quantity * 3000; // three seconds per query
    let start = Date.now();
    while( result < quantity ){
        if( fail ) return false;
        if( Date.now() - start > wait ) {
            return false;
        }
        //TODO break after some time
        // return false
    }
    return true;
}